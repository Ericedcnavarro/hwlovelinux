**11 nov 2020**

Informa de diverses particions, els seus punts de muntatge i l'espai utilitzat i disponible de cadascuna.

[ism5230318@a20 ~]$ df -H
Filesystem                                                           Size  Used Avail Use% Mounted on
devtmpfs                                                             2,0G     0  2,0G   0% /dev
tmpfs                                                                2,1G  123M  1,9G   7% /dev/shm
tmpfs                                                                2,1G  1,8M  2,0G   1% /run
/dev/sda5                                                            106G   17G   84G  17% /
tmpfs                                                                2,1G  156k  2,1G   1% /tmp
tmpfs                                                                401M  127k  401M   1% /run/user/102358
gandhi.informatica.escoladeltreball.org:/users/inf/hism1/ism5230318  1,0T  273G  727G  28% /home/users/inf/hism1/ism5230318/ism5230318

**Display only memory information**

Per mostrar informació sobre la memòria, especifiqueu la classe de memòria

[root@a20 ~]# sudo lshw -short -class memory
H/W path               Device     Class          Description
============================================================
/0/0                              memory         64KiB BIOS
/0/4                              memory         128KiB L1 cache
/0/5                              memory         512KiB L2 cache
/0/6                              memory         3MiB L3 cache
/0/7                              memory         4GiB System Memory
/0/7/0                            memory         4GiB DIMM DDR3 Synchronous 1400
/0/7/1                            memory         DIMM [empty]
/0/7/2                            memory         DIMM [empty]
/0/7/3                            memory         DIMM [empty]
